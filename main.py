#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from CGIHTTPServer import CGIHTTPRequestHandler
# from http.server import HTTPServer, CGIHTTPRequestHandler
import sys

PORT = 8000
if len(sys.argv) == 2:
    PORT = int(sys.argv[1])

server_address = ('', PORT)
httpd = HTTPServer(server_address, CGIHTTPRequestHandler)
httpd.serve_forever()
